# Libre Office com Estilos

Livreto com o propósito de estimular o uso de estilos na edição de textos com o 
LibreOffice Writer. Este repositório guarda os artefatos e o arquivo .odf 
correspondente.

# Licença

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
  <img alt="Licença Creative Commons" style="border-width:0" 
  src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" />
</a>

<span xmlns:dct="http://purl.org/dc/terms/" 
href="http://purl.org/dc/dcmitype/Text" property="dct:title" 
rel="dct:type">LibreOffice Writer com Estilos</span>
de <span xmlns:cc="http://creativecommons.org/ns#" 
property="cc:attributionName">Denis Alessandro Altoé Falqueto</span> está 
licenciado com uma Licença 
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative 
Commons - Atribuição-CompartilhaIgual 4.0 Internacional</a>.

Podem estar disponíveis autorizações adicionais às concedidas no âmbito desta 
licença em <a xmlns:cc="http://creativecommons.org/ns#" 
href="mailto://denisfalqueto@gmail.com" 
rel="cc:morePermissions">mailto://denisfalqueto@gmail.com</a>.
